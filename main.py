import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.download = 0
        self.file = ""

    # Descarga el documento si no se ha descargado antes
    def retrieve(self):
        if not self.download:
            print(f"Descargando {self.url}")
            urllib.request.urlretrieve(self.url)
            output = urllib.request.urlopen(self.url).read().decode('utf-8')
            self.file = output
            self.download = 1

    # Muestra el contenido descargado
    def show(self):
        if not self.download:
            self.retrieve()
        print(self.file)

    # Devuelve el contenido descargado
    def content(self):
        return self.file


class Cache:
    def __init__(self):
        self.cache = {}

    # Descarga el documento si no ha sido descargado antes
    def retrieve(self, url):
        if url not in self.cache:
            r = Robot(url)
            r.retrieve()
            self.cache[url] = r.file

    # Muestra el contenido descargado
    def show(self, url):
        if url not in self.cache:
            self.retrieve(url)
        print(self.cache[url])

    # Muestra un listado de los documentos descargados
    def show_all(self):
        print(self.cache)

    # Devuelve el contenido descargado
    def content(self, url):
        return self.cache[url]


if __name__ == '__main__':
    robot1 = Robot("http://gsyc.urjc.es/")
    print(robot1.url)
    robot1.show()
    robot1.retrieve()
    robot1.retrieve()

    cache1 = Cache()
    cache1.retrieve('http://gsyc.urjc.es/')
    cache1.show('https://www.aulavirtual.urjc.es')
